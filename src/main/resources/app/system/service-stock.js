'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('stockService', ['$log', '$http', StockService]);
	
	function StockService($log, $http)
	{
		var ret = {};
		
		ret.uploadFile = function(file)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/stock/upload-file',
 				data:file,
 			});
 	   	};
 	   	
		return ret;
	}
	
})();