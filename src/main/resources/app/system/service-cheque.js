'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('chequeService', ['$log', '$http', ChequeService]);
	
	function ChequeService($log, $http)
	{
		var ret = {};
		
		ret.findChequeById = function(chequeId)
		{
			return $http({
				method : 'POST',
				url: _contextPath + '/rest/cheque/find-cheque-by-id',
				params: {'chequeId': chequeId},
			});
		};
		
		ret.findChequeAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/cheque/find-cheque-all',
 			});
 	   	};
 	   	
 	   	
 	   	ret.addCheque= function(cheque)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/cheque/add-cheque',
 				data:cheque,
 			});
 	   	};
 	   	
 	   	ret.updateCheque = function(cheque,approvedBy)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/cheque/update-cheque',
 	   			data:cheque,
 	   			params: {'approvedBy': approvedBy},
 	   		});
 	   	};
 	   	
 	   	ret.deleteChequeById = function(chequeId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/cheque/delete-cheque-by-id',
 				params: {'chequeId': chequeId},
 			});
 	   	};
		return ret;
	}
	
})();