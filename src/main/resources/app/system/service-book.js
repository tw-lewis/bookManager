'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('bookService', ['$log', '$http', BookService]);
	
	function BookService($log, $http)
	{
		var ret = {};
		
		ret.findBookAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/book/find-book-all',
 			});
 	   	};
 	   	
 	   	ret.findBookById = function(id)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/book/find-book-by-id',
 	   			params: {'bookId': id},
 	   		});
 	   	};
 	   	ret.findBookByName = function(subject)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/book/find-book-by-name',
 	   			params: {'subject': subject},
 	   		});
 	   	};
 	   	
 	   	ret.addBook = function(book,authorId,categoryId,publisherId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/book/add-book',
 				data:book,
 				params: {'authorId': authorId,'categoryId': categoryId,'publisherId': publisherId},
 			});
 	   	};
 	   	
 	   	ret.updateBook = function(book,authorId,categoryId,publisherId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/book/update-book',
 	   			data:book,
 	   			params: {'authorId': authorId,'categoryId': categoryId,'publisherId': publisherId},
 	   		});
 	   	};
 	   	
 	   	ret.deleteBookById = function(bookId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/book/delete-book-by-id',
 				params: {'bookId': bookId},
 			});
 	   	};
		return ret;
	}
	
})();