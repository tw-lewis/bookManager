'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('categoryService', ['$log', '$http', CategoryService]);
	
	function CategoryService($log, $http)
	{
		var ret = {};
		
		ret.findCategoryById = function(categoryId)
		{
			return $http({
				method : 'POST',
				url: _contextPath + '/rest/category/find-category-by-id',
				params: {'categoryId': categoryId},
			});
		};
		ret.findCategoryAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/category/find-category-all',
 			});
 	   	};
 	   	
 	   	
 	   	ret.addCategory= function(category)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/category/add-category',
 				data:category,
 			});
 	   	};
 	   	
 	   	ret.updateCategory = function(category)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/category/update-category',
 	   			data:category,
 	   		});
 	   	};
 	   	
 	   	ret.deleteCategoryById = function(categoryId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/category/delete-category-by-id',
 				params: {'categoryId': categoryId},
 			});
 	   	};
		return ret;
	}
	
})();