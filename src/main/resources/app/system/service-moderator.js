'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('moderatorService', ['$log', '$http', ModeratorService]);
	
	function ModeratorService($log, $http)
	{
		var ret = {};
		
		ret.findModeratorAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/moderator/find-moderator-all',
 			});
 	   	};
 	   	
 	   	ret.findModeratorById = function(moderatorId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/moderator/find-moderator-by-id',
 	   			params: {'moderatorId': moderatorId},
 	   		});
 	   	};
 	   	ret.findByModeratorNameAndPassword = function(userName,password)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/moderator/find-by-moderator-name-and-password',
 	   			params: {'userName': userName,'password':password},
 	   		});
 	   	};
 	   	
 	   	ret.addModerator = function(moderator)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/moderator/add-moderator',
 				data:moderator,
 			});
 	   	};
 	   	
 	   	ret.updateModerator = function(moderator)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/moderator/update-moderator',
 	   			data:moderator,
 	   		});
 	   	};
 	   	
 	   	ret.deleteModeratorById = function(moderatorId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/moderator/delete-moderator-by-id',
 				params: {'moderatorId': moderatorId},
 			});
 	   	};
		return ret;
	}
	
})();