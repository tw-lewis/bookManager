'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('shelfService', ['$log', '$http', ShelfService]);
	
	function ShelfService($log, $http)
	{
		var ret = {};
		
		ret.findShelfAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/shelf/find-shelf-all',
 			});
 	   	};
 	   	ret.findShelfById = function(shelfId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/shelf/find-shelf-by-id',
 	   			params: {'shelftId': shelfId},
 	   		});
 	   	};
 	   	ret.findShelfByUser = function(userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/shelf/find-shelf-by-user',
 	   			params: {'userId': userId},
 	   		});
 	   	};
 	   	
 	   	
 	   	ret.addShelf= function(shelf)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/shelf/add-shelf',
 				data:shelf,
 			});
 	   	};
 	   	
 	   	ret.updateShelf = function(shelf)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/shelf/update-shelf',
 	   			data:shelf,
 	   		});
 	   	};
 	   	
 	   	ret.deleteShelfById = function(shelfId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/shelf/delete-shelf-by-id',
 				params: {'shelfId': shelfId},
 			});
 	   	};
 	   	ret.readBook = function(shelfId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/shelf/read-book',
 	   			params: {'shelfId': shelfId},
 	   		});
 	   	};
		return ret;
	}
	
})();