'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('publisherService', ['$log', '$http', PublisherService]);
	
	function PublisherService($log, $http)
	{
		var ret = {};
		
		ret.findPublisherById = function(publisherId)
		{
			return $http({
				method : 'POST',
				url: _contextPath + '/rest/publisher/find-publisher-by-id',
				params: {'publisherId': publisherId},
			});
		};
		
		ret.findPublisherAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/publisher/find-publisher-all',
 			});
 	   	};
 	   	
 	   	
 	   	ret.addPublisher= function(publisher)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/publisher/add-publisher',
 				data:publisher,
 			});
 	   	};
 	   	
 	   	ret.updatePublisher = function(publisher)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/publisher/update-publisher',
 	   			data:publisher,
 	   		});
 	   	};
 	   	
 	   	ret.deletePublisherById = function(publisherId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/publisher/delete-publisher-by-id',
 				params: {'publisherId': publisherId},
 			});
 	   	};
		return ret;
	}
	
})();