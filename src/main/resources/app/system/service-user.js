'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('userService', ['$log', '$http', UserService]);
	
	function UserService($log, $http)
	{
		var ret = {};
		
		ret.findUserAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/user/find-user-all',
 			});
 	   	};
 	   	
 	   	ret.findUserById = function(userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/user/find-user-by-id',
 	   			params: {'userId': userId},
 	   		});
 	   	};
 	   	ret.findByUserNameAndPassword = function(userName,password)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/user/find-by-user-name-and-password',
 	   			params: {'userName': userName,'password':password},
 	   		});
 	   	};
 	   	
 	   	ret.addUser = function(user)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/user/add-user',
 				data:user,
 			});
 	   	};
 	   	
 	   	ret.updateUser = function(user)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/user/update-user',
 	   			data:user,
 	   		});
 	   	};
 	   	ret.updateUserApprove = function(user,approve)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/user/update-user-approve',
 	   			data:user,
 	   			params: {'approveBy': approve},
 	   		});
 	   	};
 	   	
 	   	ret.deleteUserById = function(userId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/user/delete-user-by-id',
 				params: {'userId': userId},
 			});
 	   	};
		return ret;
	}
	
})();