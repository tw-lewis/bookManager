'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('contractService', ['$log', '$http', ContractService]);
	
	function ContractService($log, $http)
	{
		var ret = {};
		
		ret.findContractAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/contract/find-contract-all',
 			});
 	   	};
 	   	ret.findCountractByStatus = function(status)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/find-contract-by-status',
 	   			params: {'status': status},
 	   		});
 	   	};
 	   	ret.findCountractById = function(contractId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/find-contract-by-id',
 	   			params: {'contractId': contractId},
 	   		});
 	   	};
 	   	ret.findCountractByUser = function(userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/find-contract-by-user',
 	   			params: {'userId': userId},
 	   		});
 	   	};
 	   	ret.findCountsractByBook = function(bookId,userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/find-contract-by-book',
 	   			params: {'bookId': bookId,'userId':userId},
 	   		});
 	   	};
 	   	
 	   	
 	   	ret.addContract= function(bookId,userId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/contract/add-contract',
 				params: {'bookId': bookId,'userId': userId},
 			});
 	   	};
 	   	ret.addContractAndAccount= function(bookId,userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/add-contract-and-account',
 	   			params: {'bookId': bookId,'userId': userId},
 	   		});
 	   	};
 	   	
 	   	ret.updateContract = function(contract)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/update-contract',
 	   			data:contract,
 	   		});
 	   	};
 	   	
 	   	ret.updateContractStatus = function(contract)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/contract/update-contract-status',
 	   			data:contract,
 	   		});
 	   	};
 	   	
 	   	ret.deleteContractById = function(contractId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/contract/delete-contract-by-id',
 				params: {'contractId': contractId},
 			});
 	   	};
		return ret;
	}
	
})();