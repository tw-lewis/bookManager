'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('accountService', ['$log', '$http', AccountService]);
	
	function AccountService($log, $http)
	{
		var ret = {};
		
		ret.findAccountAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/account/find-account-all',
 			});
 	   	};
 	   	ret.findAccountById = function(accountId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/account/find-account-by-id',
 	   			params: {'accountId': accountId},
 	   		});
 	   	};
 	   	ret.findAccountByUser = function(userId)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/account/find-account-by-user',
 	   			params: {'userId': userId},
 	   		});
 	   	};
 	   	
 	   	
 	   	ret.addAccount= function(account)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/account/add-account',
 				data:account,
 			});
 	   	};
 	   	
 	   	ret.updateAccount = function(account)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/account/update-account',
 	   			data:account,
 	   		});
 	   	};
 	   	
 	   	ret.deleteAccountById = function(accountId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/account/delete-account-by-id',
 				params: {'accountId': accountId},
 			});
 	   	};
		return ret;
	}
	
})();