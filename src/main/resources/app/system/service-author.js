'use strict';
(function()
{
	var serviceApp = angular.module('sample.service');
	serviceApp.factory('authorService', ['$log', '$http', AuthorService]);
	
	function AuthorService($log, $http)
	{
		var ret = {};
		
		ret.findAuthorById = function(authorId)
		{
			return $http({
				method : 'POST',
				url: _contextPath + '/rest/author/find-author-by-id',
				params: {'authorId': authorId},
			});
		};
		ret.findAuthorAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/author/find-author-all',
 			});
 	   	};
 	   	
 	   	
 	   	ret.addAuthor= function(author)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/author/add-author',
 				data:author,
 			});
 	   	};
 	   	
 	   	ret.updateAuthor = function(author)
 	   	{
 	   		return $http({
 	   			method : 'POST',
 	   			url: _contextPath + '/rest/author/update-author',
 	   			data:author,
 	   		});
 	   	};
 	   	
 	   	ret.deleteAuthorById = function(authorId)
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/author/delete-author-by-id',
 				params: {'authorId': authorId},
 			});
 	   	};
		return ret;
	}
	
})();