'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('categoryContentContrl',function($scope,$uibModal,$state,categoryService){
		categoryService.findCategoryAll().then(function(data){
			$scope.categorys=data.data;
		});
		
		 $scope.choose = function(categoryId){
			 $scope.id = categoryId;
		 }
		
		 $scope.Add = function(){
			 var modalInstance = $uibModal.open({
			      templateUrl: _applicationPath + 'category/category-add.htm' ,
			      controller: 'AddCategoryCtrl',
			});
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 categoryService.findCategoryById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'category/category-update.htm' ,
						      controller: 'UpdateCategoryCtrl',
						      resolve: {
						    	  category: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 categoryService.deleteCategoryById(id).then(function(data){
					 $state.go('moderator-category',null,{reload:true});
				 });
			 }
			 
		 }
		
	});
	
	app.controller('AddCategoryCtrl', function ($scope, $uibModalInstance,categoryService,$state) {
		
		 $scope.OK = function (category) {
			 categoryService.addCategory(category).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-category',null,{reload:true});
			 });
		 }
		
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	app.controller('UpdateCategoryCtrl', function ($scope,$uibModalInstance,categoryService,category,$state) {
		$scope.category = category;
		$scope.OK = function (category) {
			categoryService.updateCategory(category).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-category',null,{reload:true});
			 });
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
	
})();
