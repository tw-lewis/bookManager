'use strict';
(function() {
	var stdRouteApp = angular.module('std.route.app');

	stdRouteApp.config([ '$stateProvider', '$urlRouterProvider', config ]);

	function config($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/moderator');
		$stateProvider.state('moderator',
				{
					url : '/moderator',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/moderator/moderator-content.htm',
						}
					},

				}).state('moderator-author',
				{
					url : '/moderator/author',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/author/author-content.htm',
						}
					},

				}).state('moderator-category',
				{
					url : '/moderator/category',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/category/category-content.htm',
						}
					},

				}).state('moderator-publisher',
				{
					url : '/moderator/publisher',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/publisher/publisher-content.htm',
						}
					},

				}).state('moderator-cheque',
				{
					url : '/moderator/cheque',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/cheque/cheque-content.htm',
						}
					},

				}).state('moderator-moderatorInfo',
				{
					url : '/moderator/moderatorInfo',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/moderator/moderatorInfo-content.htm',
						}
					},

				}).state('moderator-contract',
				{
					url : '/moderator/contract',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/contract/contract-content.htm',
						}
					},

				}).state('moderator-account',
				{
					url : '/moderator/account',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/account/account-content.htm',
						}
					},

				}).state('moderator-stock',
				{
					url : '/moderator/stock',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/stock/stock-content.htm',
						}
					},

				}).state('moderator-user',
				{
					url : '/moderator/user',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/user/user-content.htm',
						}
					},

				}).state('moderator-statistics',
				{
					url : '/moderator/statistics',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/moderator-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/statistics/statistics-content.htm',
						}
					},

				});

	}

})();
