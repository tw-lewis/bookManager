'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('moderatorHeadController',function($scope,$rootScope,moderatorService,$state){
		$rootScope.loginUser=$rootScope.loginUser;
		$scope.logout = function(){
			sessionStorage.clear();
			$state.go('booklist');
		}
		if(sessionStorage.getItem("user")==""||sessionStorage.getItem("user")==null){
			$state.go('booklist');
		}
		$scope.author = function(){
			$state.go('moderator-author');
		}
		$scope.category = function(){
			$state.go('moderator-category');
		}
		$scope.publisher = function(){
			$state.go('moderator-publisher');
		}
		$scope.cheque = function(){
			$state.go('moderator-cheque');
		}
		$scope.moderator = function(){
			$state.go('moderator-moderatorInfo');
		}
		$scope.contract = function(){
			$state.go('moderator-contract');
		}
		$scope.account = function(){
			$state.go('moderator-account');
		}
		$scope.stock = function(){
			$state.go('moderator-stock');
		}
		$scope.user = function(){
			$state.go('moderator-user');
		}
		$scope.statistics = function(){
			$state.go('moderator-statistics');
		}
		
	});
	
	app.controller('modeContentContrl', function($scope,$rootScope,bookService) {
		 bookService.findBookAll().then(function(resault){
			 $scope.books =resault.data;
		 });
	});
	app.controller('moderatorContentContrl',function($scope,$uibModal,$state,moderatorService){
		moderatorService.findModeratorAll().then(function(data){
			$scope.moderators=data.data;
		});
		$scope.choose = function(moderatorId){
			 $scope.id = moderatorId;
		 }
		
		 $scope.Add = function(){
			 var modalInstance = $uibModal.open({
			      templateUrl: _applicationPath + 'moderator/moderator-add.htm' ,
			      controller: 'AddModeratorCtrl',
			});
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 moderatorService.findModeratorById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'moderator/moderator-update.htm' ,
						      controller: 'UpdateModeratorCtrl',
						      resolve: {
						    	  moderator: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 moderatorService.deleteModeratorById(id).then(function(data){
					 $state.go('moderator-moderatorInfo',null,{reload:true});
				 });
			 }
			 
		 }
		
	});
	
	app.controller('AddModeratorCtrl', function ($scope, $uibModalInstance,moderatorService,$state) {
		 $scope.OK = function (moderator) {
			 moderatorService.addModerator(moderator).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-moderatorInfo',null,{reload:true});
			 });
			 
		 }
		
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	app.controller('UpdateModeratorCtrl', function ($scope,$uibModalInstance,moderatorService,moderator,$state) {
		$scope.moderator = moderator;
		$scope.rePassword = moderator.password;
		$scope.OK = function (moderator) {
			if($scope.moderator.password==$scope.rePassword){
				moderatorService.updateModerator(moderator).then(function(data){
					$uibModalInstance.dismiss('cancel');
					$state.go('moderator-moderatorInfo',null,{reload:true});
				});
			}
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
	
	
})();
