'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('stockContentContrl',function($scope,$uibModal,$state,bookService){
		bookService.findBookAll().then(function(data){
			$scope.books=data.data;
		});
		
		 $scope.choose = function(bookId){
			 $scope.id = bookId;
		 }
		
		 $scope.Add = function(){
			 var modalInstance = $uibModal.open({
			      templateUrl: _applicationPath + 'stock/stock-add.htm' ,
			      controller: 'AddStockCtrl',
			});
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 bookService.findBookById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'stock/stock-update.htm' ,
						      controller: 'UpdateStockCtrl',
						      resolve: {
						    	  book: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				bookService.deleteBookById(id).then(function(data){
					 $state.go('moderator-stock',null,{reload:true});
				 });
			 }
			 
		 }
		
	});
	app.controller('AddStockCtrl', function ($scope, $uibModalInstance,bookService,$state,authorService,categoryService,publisherService,stockService,Upload) {
		authorService.findAuthorAll().then(function(data){
			$scope.authors=data.data;
		});
		categoryService.findCategoryAll().then(function(data){
			$scope.categorys=data.data;
		});
		publisherService.findPublisherAll().then(function(data){
			$scope.publishers=data.data;
		});
		
		 $scope.OK = function (book) {
			 var file = $scope.file.foles[0];
			 var authorId = $scope.book.author.authorId;
			 var categoryId = $scope.book.category.categoryId;
			 var publisherId = $scope.book.publisher.publisherId;
			 bookService.addBook(book,authorId,categoryId,publisherId).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-stock',null,{reload:true});
			 });
		 }
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	
	
	app.controller('UpdateStockCtrl', function ($scope,$uibModalInstance,bookService,book,$state,authorService,categoryService,publisherService) {
		authorService.findAuthorAll().then(function(data){
			$scope.authors=data.data;
		});
		categoryService.findCategoryAll().then(function(data){
			$scope.categorys=data.data;
		});
		publisherService.findPublisherAll().then(function(data){
			$scope.publishers=data.data;
		});
		$scope.book = book;
		$scope.book.author.authorId=book.author.authorId;
		$scope.book.category.categoryId=book.category.categoryId;
		$scope.book.publisher.publisherId=book.publisher.publisherId;
		$scope.OK = function (book) {
			var authorId = $scope.book.author.authorId;
			var categoryId = $scope.book.category.categoryId;
			var publisherId = $scope.book.publisher.publisherId;
			bookService.updateBook(book,authorId,categoryId,publisherId).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-stock',null,{reload:true});
			 });
		}
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
	
})();
