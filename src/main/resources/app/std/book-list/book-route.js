'use strict';
(function()
{
	var stdRouteApp = angular.module('std.route.app');
	
	stdRouteApp.config(['$stateProvider','$urlRouterProvider', config]);
	
	function config($stateProvider, $urlRouterProvider)
	{
		$urlRouterProvider.otherwise('/booklist');
		$stateProvider.state('booklist',
		{
			url: '/booklist',
			views:
			{
				'header': { templateUrl: _applicationPath + '/template/book-header.htm' },
				'footer': { templateUrl: _applicationPath + '/template/footer.htm' },
				'content' : 
				{ 
					templateUrl: _applicationPath + '/book-list/book-content.htm',
				}
			},
			
		});
	}
	                    

	
})();
