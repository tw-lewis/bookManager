'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('authorContentContrl',function($scope,authorService,$uibModal,$state){
		authorService.findAuthorAll().then(function(data){
			$scope.authors=data.data;
		});
		
		 $scope.choose = function(authorId){
			 $scope.id = authorId;
		 }
		
		 $scope.Add = function(){
			 var modalInstance = $uibModal.open({
			      templateUrl: _applicationPath + 'author/author-add.htm' ,
			      controller: 'AddAuthorCtrl',
			});
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 authorService.findAuthorById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'author/author-update.htm' ,
						      controller: 'UpdateAuthorCtrl',
						      resolve: {
						          author: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 authorService.deleteAuthorById(id).then(function(data){
					 $state.go('moderator-author',null,{reload:true});
				 });
			 }
			 
		 }
		 
		 
		
	});
	
	
	app.controller('AddAuthorCtrl', function ($scope, $uibModalInstance,authorService,$state) {
	
		 $scope.OK = function (author) {
			 authorService.addAuthor(author).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-author',null,{reload:true});
			 });
		 }
		
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	app.controller('UpdateAuthorCtrl', function ($scope,$uibModalInstance,authorService,author,$state) {
		$scope.author = author;
		$scope.OK = function (author) {
			authorService.updateAuthor(author).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-author',null,{reload:true});
			 });
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
	
	
})();
