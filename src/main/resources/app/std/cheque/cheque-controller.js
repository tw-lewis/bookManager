'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('chequeContentContrl',function($scope,$rootScope,chequeService,$state){
		chequeService.findChequeAll().then(function(data){
			$scope.cheques=data.data;
		});
		
		$scope.choose = function(chequeId){
			 $scope.id = chequeId;
		 }
		
		$scope.Approve = function(id){
			if(id!=""&&id!=null){
				chequeService.findChequeById(id).then(function(data){
					var approveBy = sessionStorage.getItem("user");
					chequeService.updateCheque(data.data,approveBy).then(function(resault){
						$state.go('moderator-cheque',null,{reload:true});
					});
				});
			}
		}
		
	});
	
})();
