'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('accountContentContrl',function($scope,$uibModal,accountService,userService,$state){
		userService.findUserAll().then(function(data){
			$scope.users=data.data;
		});
		
		$scope.chooseUser = function(userId){
			$scope.count = 0;
			if(userId!=""&&userId!=null){
				accountService.findAccountByUser(userId).then(function(data){
					$scope.accounts = data.data;
					for(var i = 0;i<data.data.length;i++){
						$scope.count+=data.data[i].amount;
					}
				});
				$scope.uId = userId;
			}
		}
		
		$scope.chooseAccount = function(accountId){
			$scope.id = accountId;
		}
		
		$scope.Add = function(uId){
			if(uId!=""&&uId!=null){
				userService.findUserById(uId).then(function(data){
					var modalInstance = $uibModal.open({
						templateUrl: _applicationPath + 'account/account-add.htm' ,
						controller: 'AddAccountCtrl',
						resolve: {
							user:function(){
								return data.data;
							}
						}
					});
				});
			}
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 accountService.findAccountById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'account/account-update.htm' ,
						      controller: 'UpdateaccountCtrl',
						      resolve: {
						    	  account: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 accountService.deleteAccountById(id).then(function(data){
					 $state.go('moderator-account',null,{reload:true});
				 });
			 }
			 
		 }
		 
		 
		
	});
	
	
	app.controller('AddAccountCtrl', function ($scope, $uibModalInstance,accountService,$state,user) {
		 $scope.account={};
		 $scope.account.user=user;
		 $scope.OK = function (account) {
			 accountService.addAccount(account).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-account',null,{reload:true});
			 });
		 }
		
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	app.controller('UpdateaccountCtrl', function ($scope,$uibModalInstance,accountService,account,$state) {
		$scope.account = account;
		$scope.OK = function (account) {
			accountService.updateAccount(account).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-account',null,{reload:true});
			 });
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
})();
