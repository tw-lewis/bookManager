'use strict';
(function() {
	var app = angular.module('std.app');
	app.controller('headController',function($scope,$uibModal,moderatorService) {
		$scope.open = function(index){
			if(index==1){
				var modalInstance = $uibModal.open({
				      templateUrl: _applicationPath + 'user/coustomer-register.htm' ,
				      controller: 'CoustomerCtrl',
				});
			}else if(index==2){
				var modalInstance = $uibModal.open({
				      templateUrl: _applicationPath + 'user/moderator-login.htm' ,
				      controller: 'CoustomerCtrl',
				});
			}else if(index==3){
				var modalInstance = $uibModal.open({
				      templateUrl: _applicationPath + 'user/coustomer-login.htm' ,
				      controller: 'CoustomerCtrl',
				});
			}
		}
		
		
	});
	
	app.controller('CoustomerCtrl', function ($scope, $rootScope,$uibModalInstance,userService,moderatorService,$state) {
		  $scope.register = function (user) {
			  if($scope.user.password==$scope.rePassword){
				  userService.addUser(user).success(function(data){
					  $uibModalInstance.dismiss('cancel');
				  });
			  }
		  };
		  
		  $scope.OK = function (user,index) {
			  var userName = user.userName;
			  var password = user.password;
			  if(index=="s"){
				  moderatorService.findByModeratorNameAndPassword(userName,password).then(function(data){
					  if(data.data!=null&&data.data!=""){
						  sessionStorage.setItem("user", data.data.userName);
						  $uibModalInstance.dismiss('cancel');
						  $state.go('moderator');
					  }else{
						  $uibModalInstance.dismiss('cancel');
					  }
				  });
			  }else if(index=="c"){
				  userService.findByUserNameAndPassword(userName,password).then(function(data){
					  if(data.data!=null&&data.data!=""){
						  sessionStorage.setItem("user", data.data.userName);
						  sessionStorage.setItem("userId", data.data.userId);
						  $uibModalInstance.dismiss('cancel');
						  $state.go('user');
					  }else{
						  $uibModalInstance.dismiss('cancel');
					  }
				  });
			  }
		  };

		  $scope.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
	});
	
	
	
	app.controller('userContentContrl',function($scope,$rootScope,userService,$uibModal,$state){
		$rootScope.loginUser=$rootScope.loginUser;
		userService.findUserAll().then(function(data){
			$scope.users=data.data;
		});
		
		 $scope.choose = function(userId){
			 $scope.id = userId;
		 }
		
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 userService.findUserById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'user/user-update.htm' ,
						      controller: 'UpdateUserCtrl',
						      resolve: {
						    	  user: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 userService.deleteUserById(id).then(function(data){
					 $state.go('moderator-user',null,{reload:true});
				 });
			 }
			 
		 }
		 $scope.Approve = function(id){
			 if(id!=""&&id!=null){
				 userService.findUserById(id).then(function(data){
					 var approve = sessionStorage.getItem("user");
					 userService.updateUserApprove(data.data,approve).then(function(resault){
						 $state.go('moderator-user',null,{reload:true});
					 });
				 });
			 }
		 }
		
	});
	
	app.controller('UpdateUserCtrl', function ($scope,$uibModalInstance,userService,user,$state) {
		$scope.user = user;
		$scope.OK = function (user) {
			userService.updateUser(user).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-user',null,{reload:true});
			 });
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
	
	
	app.controller('userHeadController',function($scope,$rootScope,bookService,$state){
		$rootScope.loginUser=$rootScope.loginUser;
		$scope.logout = function(){
			sessionStorage.clear();
			$state.go('booklist');
		}
		if(sessionStorage.getItem("user")==""||sessionStorage.getItem("user")==null){
			$state.go('booklist');
		}
		$scope.book = function(){
			$state.go('user');
		}
		$scope.shelf = function(){
			$state.go('user-shelf');
		}
		$scope.transaction = function(){
			$state.go('user-transaction');
		}
		$scope.about = function(){
			
		}
	});
	
	
	app.controller('userBookcontentContrl', function($scope,$rootScope,bookService,$uibModal) {
		 bookService.findBookAll().then(function(resault){
			 $scope.id = '';
			 $scope.books =resault.data;
		 });
		 
		 
		 $scope.choose = function(bookId){
			 $scope.id = bookId;
		 }
		 $scope.buy = function(bookId){
			 if(bookId!=null&&bookId!=''&&$rootScope.loginUser!=""){
				 bookService.findBookById(bookId).then(function(data){
					 var modalInstance = $uibModal.open({
						 templateUrl: _applicationPath + 'user/user-book-by.htm' ,
						 controller: 'UserBookByCtrl',
						 resolve: {
							 book: function () {
								 return data.data;
							 }
						 }
					 });
				 });
			 }
		 }
		 
	});
	
	app.controller('UserBookByCtrl', function($scope,$rootScope,contractService,accountService,$uibModalInstance,book) {
		$scope.book = book;
		var userId = sessionStorage.getItem("userId");
		$scope.OK = function (book) {
			contractService.findCountsractByBook(book.bookId,userId).then(function(resault){
				if(resault.data==null||resault.data==""){
					if($scope.mode=='1'){
						var count = 0;
						accountService.findAccountByUser(userId).then(function(data){
							for(var i = 0;i<data.data.length;i++){
								count+=data.data[i].amount;
							}
							if(count>=book.price){
								contractService.addContractAndAccount(book.bookId,userId).then(function(data){
									$uibModalInstance.dismiss('cancel');
								});
							}else{
								alert("Not enough cash or the selectd book has been bought already!");
								$uibModalInstance.dismiss('cancel');
							}
						});
					}else if($scope.mode=='2'){
						contractService.addContract(book.bookId,userId).then(function(data){
							$uibModalInstance.dismiss('cancel');
						});
					}
				}else{
					alert("Not enough cash or the selectd book has been bought already!");
					$uibModalInstance.dismiss('cancel');
				}
			});
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	});
	
})();
