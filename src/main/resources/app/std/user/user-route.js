'use strict';
(function() {
	var stdRouteApp = angular.module('std.route.app');

	stdRouteApp.config([ '$stateProvider', '$urlRouterProvider', config ]);

	function config($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/user');
		$stateProvider.state('user',
				{
					url : '/user',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/user-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/user/user-book-content.htm',
						}
					},

				}).state('user-shelf',
				{
					url : '/user/shelf',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/user-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/shelf/shelf-content.htm',
						}
					},

				}).state('user-transaction',
				{
					url : '/user/transaction',
					views : {
						'header' : {
							templateUrl : _applicationPath
									+ '/template/user-header.htm'
						},
						'footer' : {
							templateUrl : _applicationPath
									+ '/template/footer.htm'
						},
						'content' : {
							templateUrl : _applicationPath
									+ '/transaction/transaction-content.htm',
						}
					},

				});

	}

})();
