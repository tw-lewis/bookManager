'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('shelfcontentContrl',function($scope,$rootScope,shelfService,$state){
		var id = sessionStorage.getItem("userId");
		shelfService.findShelfByUser(id).then(function(data){
			$scope.shelfs = data.data;
		});
		
		$scope.read = function(id){
			if(id!=""&&id!=null){
				shelfService.readBook(id).then(function(data){
					$state.go('user-shelf',null,{reload:true});
				});
			}
		}
		
		$scope.choose = function(bookId){
			$scope.id = bookId;
		}
		
	});
	
})();
