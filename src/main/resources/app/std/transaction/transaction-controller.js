'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('transactioncontentContrl',function($scope,$rootScope,contractService,accountService,$state){
		var userId = sessionStorage.getItem("userId")
		contractService.findCountractByUser(userId).then(function(data){
			$scope.contracts = data.data;
		});
		
		accountService.findAccountByUser(userId).then(function(data){
			$scope.accounts = data.data;
		});
		
		
		$scope.choose = function(contractId){
			$scope.id = contractId;
		}
		
		$scope.contract = function(id){
			if(id!=""&&id!=null){
				contractService.findCountractById(id).then(function(data){
					contractService.updateContractStatus(data.data).then(function(resault){
						$state.go('user-transaction',null,{reload:true});
					});
				});
			}
		}
		
		
	});
	
})();
