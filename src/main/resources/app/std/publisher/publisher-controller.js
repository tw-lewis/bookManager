'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('publisherContentContrl',function($scope,$uibModal,$state,publisherService){
		publisherService.findPublisherAll().then(function(data){
			$scope.publishers=data.data;
		});
		
		 $scope.choose = function(publisherId){
			 $scope.id = publisherId;
		 }
		
		 $scope.Add = function(){
			 var modalInstance = $uibModal.open({
			      templateUrl: _applicationPath + 'publisher/publisher-add.htm' ,
			      controller: 'AddPublisherCtrl',
			});
		 }
		 $scope.Update = function(id){
			 if(id!=""&&id!=null){
				 publisherService.findPublisherById(id).then(function(data){
					 if(data.data!=""&&data.data!=null){
						 var modalInstance = $uibModal.open({
						      templateUrl: _applicationPath + 'publisher/publisher-update.htm' ,
						      controller: 'UpdatePublisherCtrl',
						      resolve: {
						    	  publisher: function () {
						            return data.data;
						          }
						     }
						});
					 }
				 });
			 }
		 }
		 $scope.Delete = function(id){
			 if(id!=""&&id!=null){
				 publisherService.deletePublisherById(id).then(function(data){
					 $state.go('moderator-publisher',null,{reload:true});
				 });
			 }
			 
		 }
		
	});
	
	app.controller('AddPublisherCtrl', function ($scope, $uibModalInstance,publisherService,$state) {
		
		 $scope.OK = function (publisher) {
			 publisherService.addPublisher(publisher).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-publisher',null,{reload:true});
			 });
		 }
		
		 $scope.cancel = function () {
			    $uibModalInstance.dismiss('cancel');
			  };
		
	});
	app.controller('UpdatePublisherCtrl', function ($scope,$uibModalInstance,publisherService,publisher,$state) {
		$scope.publisher = publisher;
		$scope.OK = function (publisher) {
			publisherService.updatePublisher(publisher).then(function(data){
				 $uibModalInstance.dismiss('cancel');
				 $state.go('moderator-publisher',null,{reload:true});
			 });
		}
		
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		
	});
	
})();
