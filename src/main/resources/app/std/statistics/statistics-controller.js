'use strict';
(function() {
	var app = angular.module('std.app');
	
	app.controller('statisticsContentContrl',function($scope,$uibModal,$state,accountService,contractService){
		accountService.findAccountAll().then(function(data){
			var count = 0;
			for (var i = 0; i < data.data.length; i++) {
				count+=data.data[i].amount;
			}
			$scope.amount=count;
		});
		
		contractService.findCountractByStatus("1").then(function(data){
			$scope.completed=data.data.length;
		});
		contractService.findCountractByStatus("3").then(function(data){
			$scope.voided=data.data.length;
		});
		
	});
	
})();
