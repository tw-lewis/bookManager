package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.ChequeBean;
import com.fhk.sample.service.ChequeService;

@RestController
@RequestMapping(value="/rest/cheque")
public class ChequeController {
	
	@Inject
	private ChequeService chequeService;
	
	@RequestMapping(value="find-cheque-by-id",method=RequestMethod.POST)
	public ChequeBean findChequeById(@RequestParam(value="chequeId",required=true)final Long chequerId) {
		return chequeService.findById(chequerId);
	}
	
	@RequestMapping(value="find-cheque-all",method=RequestMethod.POST)
	public List<ChequeBean> findChequeAll() {
		return chequeService.findAll();
	}
	
	@RequestMapping(value="update-cheque",method=RequestMethod.POST)
	public Map<String, String> updateCheque(@RequestBody ChequeBean cheque,@RequestParam(value="approvedBy",required=true)String approvedBy) {
		chequeService.updateCheque(cheque,approvedBy);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="add-cheque",method=RequestMethod.POST)
	public Map<String, String> addCheque(@RequestBody ChequeBean cheque) {
		chequeService.add(cheque);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-cheque-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteChequeById(@RequestParam(value="chequeId",required=true)final Long chequeId) {
		chequeService.deleteById(chequeId);
		return new HashMap<String, String>();
	}
	

}
