package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.PublisherBean;
import com.fhk.sample.service.PublisherService;

@RestController
@RequestMapping(value="/rest/publisher")
public class PublisherController {
	
	@Inject
	private PublisherService publisherService;
	
	@RequestMapping(value="find-publisher-by-id",method=RequestMethod.POST)
	public PublisherBean findPublisherById(@RequestParam(value="publisherId",required=true)final Long publisherId) {
		return publisherService.findById(publisherId);
	}
	
	@RequestMapping(value="find-publisher-all",method=RequestMethod.POST)
	public List<PublisherBean> findPublisherAll() {
		return publisherService.findAll();
	}
	
	@RequestMapping(value="add-publisher",method=RequestMethod.POST)
	public Map<String, String> addPublisher(@RequestBody PublisherBean publisher) {
		publisherService.add(publisher);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-publisher",method=RequestMethod.POST)
	public Map<String, String> updatePublisher(@RequestBody PublisherBean publisher) {
		publisherService.add(publisher);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-publisher-by-id",method=RequestMethod.POST)
	public Map<String, String> deletePublisherById(@RequestParam(value="publisherId",required=true)final Long publisherId) {
		publisherService.deleteById(publisherId);
		return new HashMap<String, String>();
	}
	

}
