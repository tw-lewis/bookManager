package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.CategoryBean;
import com.fhk.sample.service.CategoryService;

@RestController
@RequestMapping(value="/rest/category")
public class CategoryController {
	
	@Inject
	private CategoryService categoryService;
	
	@RequestMapping(value="find-category-by-id",method=RequestMethod.POST)
	public CategoryBean findCategoryById(@RequestParam(value="categoryId",required=true)final Long categoryId) {
		return categoryService.findById(categoryId);
	}
	
	@RequestMapping(value="find-category-all",method=RequestMethod.POST)
	public List<CategoryBean> findCategoryAll() {
		return categoryService.findAll();
	}
	
	@RequestMapping(value="add-category",method=RequestMethod.POST)
	public Map<String, String> addCategory(@RequestBody CategoryBean category) {
		categoryService.add(category);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-category",method=RequestMethod.POST)
	public Map<String, String> updateCategory(@RequestBody CategoryBean category) {
		categoryService.add(category);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-category-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteCategoryById(@RequestParam(value="categoryId",required=true)final Long categoryId) {
		categoryService.deleteById(categoryId);
		return new HashMap<String, String>();
	}
	

}
