package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.CashAccountTransactionBean;
import com.fhk.sample.service.CashAccountTransactionService;

@RestController
@RequestMapping(value="/rest/account")
public class AccountController {
	
	@Inject
	private CashAccountTransactionService accountService;
	
	@RequestMapping(value="find-account-by-id",method=RequestMethod.POST)
	public CashAccountTransactionBean findAccountById(@RequestParam(value="accountId",required=true)final Long accountId) {
		return accountService.findById(accountId);
	}
	@RequestMapping(value="find-account-by-user",method=RequestMethod.POST)
	public List<CashAccountTransactionBean> findAccountByUser(@RequestParam(value="userId",required=true) Long userId) {
		return accountService.findByUser(userId);
	}
	
	@RequestMapping(value="find-account-all",method=RequestMethod.POST)
	public List<CashAccountTransactionBean> findAccountAll() {
		return accountService.findAll();
	}
	
	@RequestMapping(value="add-account",method=RequestMethod.POST)
	public Map<String, String> addAccount(@RequestBody CashAccountTransactionBean account) {
		accountService.add(account);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-account",method=RequestMethod.POST)
	public Map<String, String> updateAccount(@RequestBody CashAccountTransactionBean account) {
		accountService.add(account);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-account-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteAccountById(@RequestParam(value="accountId",required=true)final Long accountId) {
		accountService.deleteById(accountId);
		return new HashMap<String, String>();
	}
	

}
