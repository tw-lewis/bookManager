package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.AuthorBean;
import com.fhk.sample.service.AuthorService;

@RestController
@RequestMapping(value="/rest/author")
public class AuthorController {
	
	@Inject
	private AuthorService authorService;
	
	@RequestMapping(value="find-author-by-id",method=RequestMethod.POST)
	public AuthorBean findAuthorById(@RequestParam(value="authorId",required=true)final Long authorId) {
		return authorService.findById(authorId);
	}
	
	@RequestMapping(value="find-author-all",method=RequestMethod.POST)
	public List<AuthorBean> findAuthorAll() {
		return authorService.findAll();
	}
	
	@RequestMapping(value="add-author",method=RequestMethod.POST)
	public Map<String, String> addAuthor(@RequestBody AuthorBean author) {
		authorService.add(author);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-author",method=RequestMethod.POST)
	public Map<String, String> updateAuthor(@RequestBody AuthorBean author) {
		authorService.add(author);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-author-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteAuthorById(@RequestParam(value="authorId",required=true)final Long authorId) {
		authorService.deleteById(authorId);
		return new HashMap<String, String>();
	}
	

}
