package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.ContractBean;
import com.fhk.sample.service.ContractService;
import com.fhk.sample.util.Constant;

@RestController
@RequestMapping(value="/rest/contract")
public class ContractController {
	
	@Inject
	private ContractService contractService;
	
	@RequestMapping(value="find-contract-all",method=RequestMethod.POST)
	public List<ContractBean> findContractAll() {
		return contractService.findAll();
	}
	
	@RequestMapping(value="add-contract",method=RequestMethod.POST)
	public Map<String, String> addContract(@RequestParam(value="bookId",required=true)final Long bookId,@RequestParam(value="userId",required=true)final Long userId) {
		contractService.addContract(bookId, userId);
		return new HashMap<String, String>();
	}
	@RequestMapping(value="add-contract-and-account",method=RequestMethod.POST)
	public Map<String, String> addContractAndAccount(@RequestParam(value="bookId",required=true)final Long bookId,@RequestParam(value="userId",required=true)final Long userId) {
		contractService.addContractAndAccount(bookId, userId);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-contract",method=RequestMethod.POST)
	public Map<String, String> updateContract(@RequestBody ContractBean contract) {
		contractService.add(contract);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-contract-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteContractById(@RequestParam(value="contractId",required=true)final Long contractId) {
		contractService.deleteById(contractId);
		return new HashMap<String, String>();
	}
	@RequestMapping(value="find-contract-by-status",method=RequestMethod.POST)
	public List<ContractBean> findCountractByStatus(@RequestParam(value="status",required=true)final String status) {
		return contractService.findCountractByStatus(status);
	}
	@RequestMapping(value="find-contract-by-id",method=RequestMethod.POST)
	public ContractBean findCountractById(@RequestParam(value="contractId",required=true)final Long contractId) {
		return contractService.findById(contractId);
	}
	@RequestMapping(value="find-contract-by-user",method=RequestMethod.POST)
	public List<ContractBean> findCountractByUser(@RequestParam(value="userId",required=true)Long userId) {
		return contractService.findCountractByUser(userId);
	}
	
	@RequestMapping(value="update-contract-status",method=RequestMethod.POST)
	public Map<String, String> updateContractStatus(@RequestBody ContractBean contract) {
		contractService.updateContractStatus(contract);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="find-contract-by-book",method=RequestMethod.POST)
	public ContractBean findCountsractByBook(@RequestParam(value="bookId",required=true)Long bookId,@RequestParam(value="userId",required=true)Long userId) {
		return contractService.findCountsractByBook(bookId, Constant.CONTRACT_STATUS_APPROVED,userId);
	}
	
}
