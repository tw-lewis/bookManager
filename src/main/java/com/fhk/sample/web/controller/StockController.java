package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.BookBean;
import com.fhk.sample.service.BookService;

@RestController
@RequestMapping(value="/rest/stock")
public class StockController {
	
	@Inject
	private BookService bookService;
	
	@RequestMapping(value="find-stock-all",method=RequestMethod.POST)
	public List<BookBean> findStockAll() {
		return bookService.findAll();
	}
	
	@RequestMapping(value="add-stock",method=RequestMethod.POST)
	public Map<String, String> addStock(@RequestBody BookBean stock) {
		bookService.add(stock);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-stock",method=RequestMethod.POST)
	public Map<String, String> updateStock(@RequestBody BookBean stock) {
		bookService.add(stock);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-stock-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteStockById(@RequestParam(value="stockId",required=true)final Long stockId) {
		bookService.deleteById(stockId);
		return new HashMap<String, String>();
	}
	
}
