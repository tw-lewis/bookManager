package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.ModeratorBean;
import com.fhk.sample.service.ModeratorService;
import com.fhk.sample.util.Constant;

@RestController
@RequestMapping(value="/rest/moderator")
public class ModeratorController {

	@Inject
	private ModeratorService moderatorService;
	
	@RequestMapping(value="find-by-moderator-name-and-password",method=RequestMethod.POST)
	public ModeratorBean findByModeratorNameAndPassword(@RequestParam(value="userName",required=true)final String userName,@RequestParam(value="password",required=true)final String password) {
		return moderatorService.findByUserNameAndPasswordAndEnable(userName, password, Constant.MODERATOR_ENABLE_APPROVED);
	}
	
	@RequestMapping(value="find-moderator-by-id",method=RequestMethod.POST)
	public ModeratorBean findModeratorById(@RequestParam(value="moderatorId",required=true)final Long moderatorId) {
		return moderatorService.findById(moderatorId);
	}
	
	@RequestMapping(value="find-moderator-all",method=RequestMethod.POST)
	public List<ModeratorBean> findModeratorAll() {
		return moderatorService.findAll();
	}
	
	@RequestMapping(value="add-moderator",method=RequestMethod.POST)
	public Map<String, String> addModerator(@RequestBody ModeratorBean moderator) {
		moderatorService.add(moderator);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-moderator",method=RequestMethod.POST)
	public Map<String, String> updateModerator(@RequestBody ModeratorBean moderator) {
		moderatorService.update(moderator);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-moderator-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteModeratorById(@RequestParam(value="moderatorId",required=true)final Long moderatorId) {
		moderatorService.deleteById(moderatorId);
		return new HashMap<String, String>();
	}
	
	
}
