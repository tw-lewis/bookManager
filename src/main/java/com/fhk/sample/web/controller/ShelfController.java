package com.fhk.sample.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.ShelfBean;
import com.fhk.sample.service.ShelfService;

@RestController
@RequestMapping(value="/rest/shelf")
public class ShelfController {
	
	@Inject
	private ShelfService shelfService;
	
	@RequestMapping(value="find-shelf-by-id",method=RequestMethod.POST)
	public ShelfBean findShelfById(@RequestParam(value="shelfId",required=true)final Long shelfId) {
		return shelfService.findById(shelfId);
	}
	@RequestMapping(value="find-shelf-by-user",method=RequestMethod.POST)
	public List<ShelfBean> findShelfByUser(@RequestParam(value="userId",required=true) Long userId) {
		return shelfService.findShelfByUser(userId);
	}
	
	@RequestMapping(value="find-shelf-all",method=RequestMethod.POST)
	public List<ShelfBean> findShelfAll() {
		return shelfService.findAll();
	}
	
	@RequestMapping(value="add-shelf",method=RequestMethod.POST)
	public Map<String, String> addShelf(@RequestBody ShelfBean shelf) {
		shelfService.add(shelf);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-shelf",method=RequestMethod.POST)
	public Map<String, String> updateShelf(@RequestBody ShelfBean shelf) {
		shelfService.add(shelf);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-shelf-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteShelfById(@RequestParam(value="shelfId",required=true)final Long shelfId) {
		shelfService.deleteById(shelfId);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="read-book",method=RequestMethod.POST)
	public Map<String, String> readBook(@RequestParam(value="shelfId",required=true)final Long shelfId) {
		shelfService.readBook(shelfId);
		return new HashMap<String, String>();
	}
	

}
