package com.fhk.sample.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.BookBean;
import com.fhk.sample.service.AuthorService;
import com.fhk.sample.service.BookService;
import com.fhk.sample.service.CategoryService;
import com.fhk.sample.service.PublisherService;

@RestController
@RequestMapping(value = "/rest/book")
public class BookController {

	@Inject
	private BookService bookService;
	@Inject
	private AuthorService authorService;
	@Inject
	private CategoryService categoryService;
	@Inject
	private PublisherService publisherService;

	@RequestMapping(value = "find-book-all", method = RequestMethod.POST)
	public List<BookBean> findBookAll() {
		return bookService.findAll();
	}

	@RequestMapping(value = "find-book-by-id", method = RequestMethod.POST)
	public BookBean findBookByName(@RequestParam(value = "bookId") Long bookId) {
		return bookService.findById(bookId);
	}

	@RequestMapping(value = "find-book-by-name", method = RequestMethod.POST)
	public List<BookBean> findBookByName(
			@RequestParam(value = "subject") String subject) {
		return bookService.findBookByName(subject);
	}

	@RequestMapping(value = "add-book", method = RequestMethod.POST)
	public Map<String, String> addBook(
			@RequestBody BookBean book,
			@RequestParam(value = "authorId", required = true) Long authorId,
			@RequestParam(value = "categoryId", required = true) Long categoryId,
			@RequestParam(value = "publisherId", required = true) Long publisherId) {
		book.setCreatedDate(new Date());
		book.setAuthor(authorService.findById(authorId));
		book.setCategory(categoryService.findById(categoryId));
		book.setPublisher(publisherService.findById(publisherId));
		bookService.add(book);
		return new HashMap<String, String>();
	}

	@RequestMapping(value = "update-book", method = RequestMethod.POST)
	public Map<String, String> updateBook(@RequestBody BookBean book,
			@RequestParam(value = "authorId", required = true) Long authorId,
			@RequestParam(value = "categoryId", required = true) Long categoryId,
			@RequestParam(value = "publisherId", required = true) Long publisherId) {
		book.setAuthor(authorService.findById(authorId));
		book.setCategory(categoryService.findById(categoryId));
		book.setPublisher(publisherService.findById(publisherId));
		bookService.update(book);
		return new HashMap<String, String>();
	}

	@RequestMapping(value = "delete-book-by-id", method = RequestMethod.POST)
	public Map<String, String> deleteBookByBookId(
			@RequestParam(value = "bookId", required = true) final Long bookId) {
		bookService.deleteById(bookId);
		return new HashMap<String, String>();
	}

}
