package com.fhk.sample.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.UserService;
import com.fhk.sample.util.Constant;

@RestController
@RequestMapping(value="/rest/user")
public class UserController {

	@Inject
	private UserService userService;
	
	@RequestMapping(value="find-by-user-name-and-password",method=RequestMethod.POST)
	public UserBean findByUserNameAndPassword(@RequestParam(value="userName",required=true)final String userName,@RequestParam(value="password",required=true)final String password) {
		return userService.findByUserNameAndPasswordAndStatusAndNumberOfRetriesBefore(userName, password, Constant.USER_STATUS_APPROVED,Constant.USER_NUMBER_OF_RETRIES_MAX);
	}
	
	@RequestMapping(value="find-user-by-id",method=RequestMethod.POST)
	public UserBean findUserById(@RequestParam(value="userId",required=true)final Long userId) {
		return userService.findById(userId);
	}
	
	@RequestMapping(value="find-user-all",method=RequestMethod.POST)
	public List<UserBean> findUserAll() {
		return userService.findAll();
	}
	
	@RequestMapping(value="add-user",method=RequestMethod.POST)
	public Map<String, String> addUser(@RequestBody UserBean user) {
		userService.addUser(user);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-user",method=RequestMethod.POST)
	public Map<String, String> updateUser(@RequestBody UserBean user) {
		user.setUpdateDate(new Date());
		userService.add(user);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="update-user-approve",method=RequestMethod.POST)
	public Map<String, String> updateUserApprove(@RequestBody UserBean user,@RequestParam(value="approveBy",required=true)final String approveBy) {
		userService.updateUserApprove(user,approveBy);
		return new HashMap<String, String>();
	}
	
	@RequestMapping(value="delete-user-by-id",method=RequestMethod.POST)
	public Map<String, String> deleteUserById(@RequestParam(value="userId",required=true)final Long userId) {
		userService.deleteById(userId);
		return new HashMap<String, String>();
	}
	
}
