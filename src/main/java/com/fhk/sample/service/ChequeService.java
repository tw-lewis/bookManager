package com.fhk.sample.service;

import com.fhk.sample.domain.entity.ChequeBean;

public interface ChequeService extends GenericService<ChequeBean, Long> {
	public ChequeBean findChequeByContract(Long contractId);
	public void updateCheque(ChequeBean entity,String approvedBy);
}
