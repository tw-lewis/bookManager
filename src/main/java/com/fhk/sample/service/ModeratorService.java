
package com.fhk.sample.service;

import com.fhk.sample.domain.entity.ModeratorBean;

public interface ModeratorService extends GenericService<ModeratorBean, Long> {
	public ModeratorBean findByUserNameAndPasswordAndEnable(String userName,String password,String enable);
}
