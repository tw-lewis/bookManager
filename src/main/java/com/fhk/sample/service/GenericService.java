package com.fhk.sample.service;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T, Q extends Serializable> {
	void add(final T entity);
	void update(final T entity);
	void deleteById(final Q id);
	
	T findById(final Q id);
	List<T> findAll();
}
