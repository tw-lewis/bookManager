package com.fhk.sample.service;

import java.util.List;

import com.fhk.sample.domain.entity.ContractBean;

public interface ContractService extends GenericService<ContractBean, Long> {
	public List<ContractBean> findCountractByStatus(String status);
	public List<ContractBean> findCountractByUser(Long userId);
	public void addContract(Long bookId,Long userId);
	public void addContractAndAccount(Long bookId,Long userId);
	public void updateContractStatus(ContractBean entity);
	public ContractBean findCountsractByBook(Long bookId,String status,Long userId);
}
