package com.fhk.sample.service;

import com.fhk.sample.domain.entity.PublisherBean;

public interface PublisherService extends GenericService<PublisherBean, Long> {

}
