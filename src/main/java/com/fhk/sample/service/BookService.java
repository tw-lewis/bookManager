package com.fhk.sample.service;

import java.util.List;

import com.fhk.sample.domain.entity.BookBean;

public interface BookService extends GenericService<BookBean, Long>{
	/**
	 * 根据书名查询
	 * @param subject
	 * @return
	 */
	List<BookBean> findBookByName(String subject);
}
