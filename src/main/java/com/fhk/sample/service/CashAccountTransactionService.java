package com.fhk.sample.service;

import java.util.List;

import com.fhk.sample.domain.entity.CashAccountTransactionBean;

public interface CashAccountTransactionService extends GenericService<CashAccountTransactionBean, Long> {
	public List<CashAccountTransactionBean> findByUser(Long user);
}
