package com.fhk.sample.service;

import com.fhk.sample.domain.entity.UserBean;

public interface UserService extends GenericService<UserBean, Long> {
	
	/**
	 * 根据用户名和密码查询
	 * @param userName
	 * @param password
	 * @return
	 */
	public UserBean findByUserNameAndPasswordAndStatusAndNumberOfRetriesBefore(String userName,String password,String status,int numberOfRetries);
	
	public UserBean findByUserNameAndStatus(String userName,String status);
	public UserBean findByUserName(String userName);
	public void addUser(UserBean user);
	public void updateUserApprove(UserBean user,String approveBy);
}
