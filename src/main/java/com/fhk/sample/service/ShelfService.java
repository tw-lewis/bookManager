package com.fhk.sample.service;

import java.util.List;

import com.fhk.sample.domain.entity.ShelfBean;

public interface ShelfService extends GenericService<ShelfBean, Long> {
	public List<ShelfBean> findShelfByUser(Long userId);
	public void readBook(Long id);
}
