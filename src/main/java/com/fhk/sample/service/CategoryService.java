package com.fhk.sample.service;

import com.fhk.sample.domain.entity.CategoryBean;

public interface CategoryService extends GenericService<CategoryBean, Long> {

}
