package com.fhk.sample.service;

import com.fhk.sample.domain.entity.AuthorBean;

public interface AuthorService extends GenericService<AuthorBean, Long>{
	
}
