package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.BookDao;
import com.fhk.sample.domain.dao.ShelfDao;
import com.fhk.sample.domain.entity.ShelfBean;
import com.fhk.sample.service.ShelfService;
import com.fhk.sample.util.FileReadAndWriter;

@Service
class ShelfServiceImpl implements ShelfService {

	@Inject
	private ShelfDao shelfDao;
	@Inject
	private BookDao bookDao;
	
	@Override
	@Transactional
	public void add(ShelfBean entity) {
		shelfDao.save(entity);
	}

	@Override
	@Transactional
	public void update(ShelfBean entity) {
		shelfDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		shelfDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public ShelfBean findById(Long id) {
		return shelfDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ShelfBean> findAll() {
		return shelfDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public List<ShelfBean> findShelfByUser(Long userId) {
		return shelfDao.findShelfByUser(userId);
	}
	
	@Override
	@Transactional
	public void readBook(Long id) {
		ShelfBean shelfBean = shelfDao.findOne(id);
		shelfBean.setLastAccessDate(new Date());
		shelfBean.setNumberOfAccess(shelfBean.getNumberOfAccess()+1);
		shelfDao.save(shelfBean);
		Long bookId = shelfBean.getBook().getBookId();
		String fileDir = bookDao.findOne(bookId).getContent();
		if (fileDir!=null) {
			FileReadAndWriter.openFile(fileDir);
		}
	}
	
}
