package com.fhk.sample.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.CategoryDao;
import com.fhk.sample.domain.entity.CategoryBean;
import com.fhk.sample.service.CategoryService;

@Service
class CategoryServiceImpl implements CategoryService {

	@Inject
	private CategoryDao gategoryDao;
	
	@Override
	@Transactional
	public void add(CategoryBean entity) {
		gategoryDao.save(entity);
	}

	@Override
	@Transactional
	public void update(CategoryBean entity) {
		gategoryDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		gategoryDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public CategoryBean findById(Long id) {
		return gategoryDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<CategoryBean> findAll() {
		return gategoryDao.findAll();
	}

}
