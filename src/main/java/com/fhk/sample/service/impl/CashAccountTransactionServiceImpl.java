package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.CashAccountTransactionDao;
import com.fhk.sample.domain.dao.UserDao;
import com.fhk.sample.domain.entity.CashAccountTransactionBean;
import com.fhk.sample.service.CashAccountTransactionService;

@Service
class CashAccountTransactionServiceImpl implements CashAccountTransactionService{

	
	@Inject
	private CashAccountTransactionDao cashAccountTransactionDao;
	@Inject
	private UserDao	userDao;
	@Override
	
	@Transactional
	public void add(CashAccountTransactionBean entity) {
		String number = String.valueOf(new Date().getTime()).substring(0, 12);
		String str = "TTN"+number;
		entity.setTransactionNumber(str);
		entity.setCreatedDate(new Date());
		Long userId=entity.getUser().getUserId();
		entity.setUser(userDao.getOne(userId));
		cashAccountTransactionDao.save(entity);
	}

	@Override
	@Transactional
	public void update(CashAccountTransactionBean entity) {
		Long userId=entity.getUser().getUserId();
		entity.setUser(userDao.getOne(userId));
		cashAccountTransactionDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(final Long cashAccountTransactionId) {
		CashAccountTransactionBean accountTransactionBean = cashAccountTransactionDao.findOne(cashAccountTransactionId);
		if(accountTransactionBean.getAmount()>0){
			cashAccountTransactionDao.delete(cashAccountTransactionId);
		}
	}

	@Override
	@Transactional(readOnly=true)
	public CashAccountTransactionBean findById(final Long cashAccountTransactionId) {
		return cashAccountTransactionDao.findOne(cashAccountTransactionId);
	}

	@Override
	@Transactional(readOnly=true)
	public List<CashAccountTransactionBean> findAll() {
		return cashAccountTransactionDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public List<CashAccountTransactionBean> findByUser(Long userId) {
		return cashAccountTransactionDao.findByUser(userId);
	}

}
