package com.fhk.sample.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.PublisherDao;
import com.fhk.sample.domain.entity.PublisherBean;
import com.fhk.sample.service.PublisherService;

@Service
class PublisherServiceImpl implements PublisherService {

	@Inject
	private PublisherDao publisherDao;
	
	@Override
	@Transactional
	public void add(PublisherBean entity) {
		publisherDao.save(entity);
	}

	@Override
	@Transactional
	public void update(PublisherBean entity) {
		publisherDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		publisherDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public PublisherBean findById(Long id) {
		return publisherDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<PublisherBean> findAll() {
		return publisherDao.findAll();
	}

}
