package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.CashAccountTransactionDao;
import com.fhk.sample.domain.dao.ChequeDao;
import com.fhk.sample.domain.dao.ContractDao;
import com.fhk.sample.domain.dao.ShelfDao;
import com.fhk.sample.domain.dao.UserDao;
import com.fhk.sample.domain.entity.CashAccountTransactionBean;
import com.fhk.sample.domain.entity.ChequeBean;
import com.fhk.sample.domain.entity.ContractBean;
import com.fhk.sample.domain.entity.ShelfBean;
import com.fhk.sample.service.ChequeService;
import com.fhk.sample.util.Constant;

@Service
class ChequeServiceImpl implements ChequeService{

	@Inject
	private ChequeDao chequeDao;
	@Inject
	private UserDao userDao;
	@Inject
	private ContractDao contractDao;
	@Inject
	private CashAccountTransactionDao cashAccountTransactionDao;
	@Inject
	private ShelfDao shelfDao;
	
	@Override
	@Transactional
	public void add(ChequeBean entity) {
		chequeDao.save(entity);
	}

	@Override
	@Transactional
	public void update(ChequeBean entity) {
		Long userId = entity.getUser().getUserId();
		entity.setUser(userDao.findOne(userId));
		chequeDao.save(entity);
	}
	@Override
	@Transactional
	public void updateCheque(ChequeBean entity,String approvedBy) {
		entity.setApprovedBy(approvedBy);
		entity.setApprovedDate(new Date());
		entity.setStatus(Constant.CHEQUE_STATUS_APPROVED);
		update(entity);
		String number = String.valueOf(new Date().getTime()).substring(0, 12);
		String str2 = "BB"+number;
		CashAccountTransactionBean account = new CashAccountTransactionBean();
		account.setTransactionNumber(str2);
		account.setUser(entity.getUser());
		account.setAmount(-entity.getAmout());
		account.setCreatedDate(new Date());
		cashAccountTransactionDao.save(account);
		addCashAccountTransaction(entity);
		ContractBean contractBean = contractDao.findOne(entity.getContract().getContractId());
		contractBean.setApprovedBy(approvedBy);
		contractBean.setStatus(entity.getStatus());
		contractBean.setTransactionNumber(account.getTransactionNumber());
		contractDao.save(contractBean);
		ShelfBean shelfBean = new ShelfBean();
		shelfBean.setBook(contractBean.getBook());
		shelfBean.setUser(contractBean.getUser());
		shelfBean.setLastAccessDate(new Date());
		shelfBean.setNumberOfAccess(0);
		shelfDao.save(shelfBean);
	}

	public void addCashAccountTransaction(ChequeBean entity) {
		String number = String.valueOf(new Date().getTime()).substring(0, 12);
		String str2 = "CHQ"+number;
		CashAccountTransactionBean account = new CashAccountTransactionBean();
		account.setTransactionNumber(str2);
		account.setUser(entity.getUser());
		account.setAmount(entity.getAmout());
		account.setCreatedDate(new Date());
		cashAccountTransactionDao.save(account);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		chequeDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public ChequeBean findById(Long id) {
		return chequeDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ChequeBean> findAll() {
		return chequeDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public ChequeBean findChequeByContract(Long contractId) {
		return chequeDao.findChequeByContract(contractId);
	}

}
