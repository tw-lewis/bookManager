package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.UserDao;
import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.UserService;
import com.fhk.sample.util.Constant;

@Service
class UserServiceImpl implements UserService{

	@Inject
	private UserDao userDao;
	
	@Override
	@Transactional
	public void add(UserBean user) {
		userDao.save(user);
	}
	@Override
	@Transactional
	public void addUser(UserBean user) {
		if ("".equals(userDao.findByUserName(user.getUserName()))||userDao.findByUserName(user.getUserName())==null) {
			user.setCreatedDate(new Date());
			user.setUpdateDate(new Date());
			user.setStatus(Constant.USER_STATUS_PENDING);
			user.setNumberOfRetries(Constant.USER_NUMBER_OF_RETRIES_MIN);
			userDao.save(user);
		}
	}

	@Override
	@Transactional
	public void update(UserBean user) {
		userDao.save(user);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		userDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public UserBean findById(Long userId) {
		return userDao.findOne(userId);
	}

	@Override
	@Transactional(readOnly=true)
	public List<UserBean> findAll() {
		return userDao.findAll();
	}

	@Override
	@Transactional
	public UserBean findByUserNameAndPasswordAndStatusAndNumberOfRetriesBefore(String userName, String password,String status,int numberOfRetries) {
		UserBean userBean = userDao.findByUserNameAndPasswordAndStatusAndNumberOfRetriesBefore(userName, password, status,numberOfRetries);
		if("".equals(userBean)||userBean==null){
			UserBean user = userDao.findByUserNameAndStatus(userName, Constant.USER_STATUS_APPROVED);
			if (user!=null&&user.getNumberOfRetries()<Constant.USER_NUMBER_OF_RETRIES_MAX) {
				user.setNumberOfRetries(user.getNumberOfRetries() + 1);
				userDao.save(user);
			}
		}else{
			userBean.setLastLoginDate(new Date());
			userDao.save(userBean);
		}
		return userBean;
	}

	@Override
	@Transactional(readOnly=true)
	public UserBean findByUserNameAndStatus(String userName,String status) {
		return userDao.findByUserNameAndStatus(userName,status);
	}

	@Override
	@Transactional(readOnly=true)
	public UserBean findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}
	@Override
	@Transactional
	public void updateUserApprove(UserBean user, String approveBy) {
		user.setStatus(Constant.USER_STATUS_APPROVED);
		user.setApprovedBy(approveBy);
		user.setApprovedDate(new Date());;
		userDao.save(user);
	}

}
