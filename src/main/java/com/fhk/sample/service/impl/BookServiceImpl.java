package com.fhk.sample.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.BookDao;
import com.fhk.sample.domain.entity.BookBean;
import com.fhk.sample.service.BookService;

@Service
class BookServiceImpl implements BookService{

	@Inject
	private BookDao bookDao;

	
	@Override
	@Transactional(readOnly=true)
	public List<BookBean> findBookByName(String subject) {
		return bookDao.findBySubjectLike(subject);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<BookBean> findAll() {
		return bookDao.findAll();
	}

	@Override
	@Transactional
	public void add(BookBean entity) {
		bookDao.save(entity);
	}

	@Override
	@Transactional
	public void update(BookBean entity) {
		bookDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		bookDao.delete(id);
	}

	@Override
	@Transactional
	public BookBean findById(Long id) {
		return bookDao.findOne(id);
	}


}
