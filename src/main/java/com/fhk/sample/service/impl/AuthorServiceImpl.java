package com.fhk.sample.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.AuthorDao;
import com.fhk.sample.domain.entity.AuthorBean;
import com.fhk.sample.service.AuthorService;

@Service
class AuthorServiceImpl implements AuthorService{

	@Inject
	private AuthorDao authorDao;
	
	@Override
	@Transactional
	public void add(AuthorBean entity) {
		authorDao.save(entity);
	}

	@Override
	@Transactional
	public void update(AuthorBean entity) {
		authorDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		authorDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public AuthorBean findById(Long id) {
		return authorDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<AuthorBean> findAll() {
		return authorDao.findAll();
	}

}
