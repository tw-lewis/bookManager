package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.ModeratorDao;
import com.fhk.sample.domain.entity.ModeratorBean;
import com.fhk.sample.service.ModeratorService;
import com.fhk.sample.util.Constant;

@Service
class ModeratorServiceImpl implements ModeratorService {

	@Inject
	private ModeratorDao moderatorDao;
	
	@Override
	@Transactional
	public void add(ModeratorBean entity) {
		if("true".equals(entity.getEnable())){
			entity.setEnable(Constant.MODERATOR_ENABLE_APPROVED);
		}else if("false".equals(entity.getEnable())||entity.getEnable()==null){
			entity.setEnable(Constant.MODERATOR_ENABLE_APENDING);
		}
		moderatorDao.save(entity);
	}

	@Override
	@Transactional
	public void update(ModeratorBean entity) {
		if("true".equals(entity.getEnable())){
			entity.setEnable(Constant.MODERATOR_ENABLE_APPROVED);
		}else if("false".equals(entity.getEnable())||entity.getEnable()==null){
			entity.setEnable(Constant.MODERATOR_ENABLE_APENDING);
		}
		moderatorDao.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		moderatorDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public ModeratorBean findById(Long id) {
		return moderatorDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ModeratorBean> findAll() {
		return moderatorDao.findAll();
	}

	@Override
	@Transactional
	public ModeratorBean findByUserNameAndPasswordAndEnable(String userName,String password,String enable) {
		ModeratorBean moderatorBean = moderatorDao.findByUserNameAndPasswordAndEnable(userName, password, enable);
		if (moderatorBean!=null) {
			moderatorBean.setLastLoginDate(new Date());
			moderatorDao.saveAndFlush(moderatorBean);
		}
		return moderatorBean;
	}

}
