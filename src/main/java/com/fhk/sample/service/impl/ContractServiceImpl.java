package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.BookDao;
import com.fhk.sample.domain.dao.CashAccountTransactionDao;
import com.fhk.sample.domain.dao.ChequeDao;
import com.fhk.sample.domain.dao.ContractDao;
import com.fhk.sample.domain.dao.ShelfDao;
import com.fhk.sample.domain.dao.UserDao;
import com.fhk.sample.domain.entity.CashAccountTransactionBean;
import com.fhk.sample.domain.entity.ChequeBean;
import com.fhk.sample.domain.entity.ContractBean;
import com.fhk.sample.domain.entity.ShelfBean;
import com.fhk.sample.service.ContractService;
import com.fhk.sample.util.Constant;

@Service
class ContractServiceImpl implements ContractService{

	@Inject
	private ContractDao contractDao;
	@Inject
	private UserDao userDao;
	@Inject
	private BookDao bookDao;
	@Inject
	private ChequeDao chequeDao;
	@Inject
	private CashAccountTransactionDao cashAccountTransactionDao;
	@Inject
	private ShelfDao shelfDao;
	
	@Override
	@Transactional
	public void add(ContractBean entity) {
		contractDao.save(entity);
	}
	@Override
	@Transactional
	public void addContract(Long bookId,Long userId) {
		ContractBean contract = new ContractBean();
		String number = String.valueOf(new Date().getTime()).substring(0, 12);
		String str = "BB"+number;
		contract.setStatus(Constant.CONTRACT_STATUS_APENDING);
		contract.setContractNumber(str);
		contract.setCreatedDate(new Date());
		contract.setUpdateDate(new Date());
		contract.setBook(bookDao.findOne(bookId));
		contract.setUser(userDao.findOne(userId));
		contractDao.save(contract);
		ChequeBean chequeBean = new ChequeBean();
		chequeBean.setUser(contract.getUser());
		chequeBean.setContract(contract);
		chequeBean.setCreatedDate(new Date());
		chequeBean.setAmout(contract.getBook().getPrice());
		chequeBean.setStatus(contract.getStatus());
		chequeDao.save(chequeBean);
	}

	@Override
	@Transactional
	public void update(ContractBean entity) {
		Long userId = entity.getUser().getUserId();
		entity.setUser(userDao.findOne(userId));
		Long bookId = entity.getBook().getBookId();
		entity.setBook(bookDao.findOne(bookId));
		contractDao.save(entity);
	}
	@Override
	@Transactional
	public void updateContractStatus(ContractBean entity) {
		if(!"1".equals(entity.getStatus())){
			entity.setStatus("3");
		}
		entity.setVoidedDate(new Date());
		update(entity);
		ChequeBean chequeBean = chequeDao.findChequeByContract(entity.getContractId());
		chequeBean.setStatus(entity.getStatus());
		chequeDao.save(chequeBean);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		contractDao.delete(id);
	}

	@Override
	@Transactional(readOnly=true)
	public ContractBean findById(Long id) {
		return contractDao.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ContractBean> findAll() {
		return contractDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public List<ContractBean> findCountractByStatus(String status) {
		return contractDao.findCountractByStatus(status);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ContractBean> findCountractByUser(Long userId) {
		return contractDao.findCountsractByUser(userId);
	}
	@Override
	@Transactional(readOnly=true)
	public ContractBean findCountsractByBook(Long bookId,String status,Long userId) {
		return contractDao.findCountsractByBook(bookId,status,userId);
	}
	@Override
	@Transactional
	public void addContractAndAccount(Long bookId, Long userId) {
		ContractBean contract = new ContractBean();
		String contractNumber = String.valueOf(new Date().getTime()).substring(0, 12);
		contractNumber = "BB"+contractNumber;
		String transactionNumber = String.valueOf(new Date().getTime()).substring(0, 12);
		transactionNumber = "BB"+transactionNumber;
		contract.setStatus(Constant.CONTRACT_STATUS_APPROVED);
		contract.setContractNumber(contractNumber);
		contract.setCreatedDate(new Date());
		contract.setUpdateDate(new Date());
		contract.setTransactionNumber(transactionNumber);
		contract.setBook(bookDao.findOne(bookId));
		contract.setUser(userDao.findOne(userId));
		contractDao.save(contract);
		
		CashAccountTransactionBean account = new CashAccountTransactionBean();
		account.setTransactionNumber(transactionNumber);
		account.setUser(contract.getUser());
		account.setAmount(-contract.getBook().getPrice());
		account.setCreatedDate(new Date());
		cashAccountTransactionDao.save(account);
		
		ShelfBean shelfBean = new ShelfBean();
		shelfBean.setBook(contract.getBook());
		shelfBean.setUser(contract.getUser());
		shelfBean.setLastAccessDate(new Date());
		shelfBean.setNumberOfAccess(0);
		shelfDao.save(shelfBean);
	}

}
