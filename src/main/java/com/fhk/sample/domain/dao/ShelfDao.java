
package com.fhk.sample.domain.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fhk.sample.domain.entity.ShelfBean;
public interface ShelfDao extends JpaRepository<ShelfBean, Long> {
	@Query("select sb from ShelfBean sb where sb.user.userId=?1")
	public List<ShelfBean> findShelfByUser(Long userId);
}
