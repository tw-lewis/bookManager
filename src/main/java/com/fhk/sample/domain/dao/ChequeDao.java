package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fhk.sample.domain.entity.ChequeBean;
public interface ChequeDao extends JpaRepository<ChequeBean, Long> {
	@Query("select cb from ChequeBean cb where cb.contract.contractId=?1")
	public ChequeBean findChequeByContract(Long contractId);
}
