package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.UserBean;
public interface UserDao extends JpaRepository<UserBean, Long> {
	/**
	 * 根据用户名和密码查询
	 * @param UserName
	 * @param password
	 * @return
	 */
	public UserBean findByUserNameAndPasswordAndStatusAndNumberOfRetriesBefore(String userName,String password,String status,int numberOfRetries);
	/**
	 * 根据用户名查询
	 * @param UserName
	 * @return
	 */
	public UserBean findByUserNameAndStatus(String userName,String status);
	public UserBean findByUserName(String userName);
}
