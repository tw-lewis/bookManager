package com.fhk.sample.domain.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.BookBean;

public interface BookDao extends JpaRepository<BookBean, Serializable>{
	/**
	 * 根据书名查询
	 * @param subject
	 * @return
	 */
	List<BookBean> findBySubjectLike(String subject);
}
