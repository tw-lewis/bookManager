
package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.ModeratorBean;
public interface ModeratorDao extends JpaRepository<ModeratorBean, Long> {
	public ModeratorBean findByUserNameAndPasswordAndEnable(String userName,String password,String enable);
}
