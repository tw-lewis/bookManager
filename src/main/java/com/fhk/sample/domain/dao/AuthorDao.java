package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.AuthorBean;
public interface AuthorDao extends JpaRepository<AuthorBean, Long>{

}
