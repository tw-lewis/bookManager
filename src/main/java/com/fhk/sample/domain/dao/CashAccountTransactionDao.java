package com.fhk.sample.domain.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fhk.sample.domain.entity.CashAccountTransactionBean;
public interface CashAccountTransactionDao extends JpaRepository<CashAccountTransactionBean, Long>{
	@Query("select cat from CashAccountTransactionBean cat where cat.user.userId=:userId")
	public List<CashAccountTransactionBean> findByUser(@Param("userId") Long userId);
}
