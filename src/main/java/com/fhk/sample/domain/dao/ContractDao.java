package com.fhk.sample.domain.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fhk.sample.domain.entity.ContractBean;
public interface ContractDao extends JpaRepository<ContractBean, Long> {
	public List<ContractBean> findCountractByStatus(String status);
	@Query("select cb from ContractBean cb where cb.user.userId=?1")
	public List<ContractBean> findCountsractByUser(Long userId);
	@Query("select cb from ContractBean cb where cb.book.bookId=?1 and cb.status=?2 and cb.user.userId=?3")
	public ContractBean findCountsractByBook(Long bookId,String status,Long userId);
	
}
