package com.fhk.sample.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CASH_ACCOUNT_TRANSACTION")
public class CashAccountTransactionBean implements Serializable {


	private static final long serialVersionUID = 5457466784069931150L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cashAccountTransaction_seq")
	@SequenceGenerator(name="cashAccountTransaction_seq", sequenceName="cashAccountTransaction_seq")
	@Column(name="CASH_ACCOUNT_TRANSACTION_ID")
	private Long cashAccountTransactionId;
	
	@Column(name="TRANSACTION_NUMBER")
	private String transactionNumber;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="USER_INFO")
	private UserBean user;
	
	@Column(name="AMOUNT")
	private double amount;
	
	@Column(name="REMARKS")
	private String remarks;
	
	@Column(name="CREATED_DATE")
	private Date createdDate;

	public Long getCashAccountTransactionId() {
		return cashAccountTransactionId;
	}

	public void setCashAccountTransactionId(Long cashAccountTransactionId) {
		this.cashAccountTransactionId = cashAccountTransactionId;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cashAccountTransactionId == null) ? 0
						: cashAccountTransactionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CashAccountTransactionBean other = (CashAccountTransactionBean) obj;
		if (cashAccountTransactionId == null) {
			if (other.cashAccountTransactionId != null)
				return false;
		} else if (!cashAccountTransactionId
				.equals(other.cashAccountTransactionId))
			return false;
		return true;
	}
}
