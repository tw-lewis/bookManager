package com.fhk.sample.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CHEQUE")
public class ChequeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cheque_seq")
	@SequenceGenerator(name="cheque_seq", sequenceName="cheque_seq")
	@Column(name="CHEQUE_ID")
	private Long chequeId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="USER_INFO")
	private UserBean user;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CONTRACT")
	private ContractBean contract;
	
	@Column(name="CREATEDDATE")
	private Date createdDate;
	
	@Column(name="AMOUT")
	private double amout;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="APPROVEDDATE")
	private Date approvedDate;
	
	@Column(name="APPROVEDBY")
	private String approvedBy;

	public Long getChequeId() {
		return chequeId;
	}

	public void setChequeId(Long chequeId) {
		this.chequeId = chequeId;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}

	public ContractBean getContract() {
		return contract;
	}

	public void setContract(ContractBean contract) {
		this.contract = contract;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public double getAmout() {
		return amout;
	}

	public void setAmout(double amout) {
		this.amout = amout;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((chequeId == null) ? 0 : chequeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChequeBean other = (ChequeBean) obj;
		if (chequeId == null) {
			if (other.chequeId != null)
				return false;
		} else if (!chequeId.equals(other.chequeId))
			return false;
		return true;
	}
	
}
