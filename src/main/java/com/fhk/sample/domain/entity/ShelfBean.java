package com.fhk.sample.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="SHELF")
public class ShelfBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="shelf_seq")
	@SequenceGenerator(name="shelf_seq", sequenceName="shelf_seq")
	@Column(name="SHELF_ID")
	private Long shelfId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="BOOK")
	private BookBean book;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="USER_INFO")
	private UserBean user;
	
	@Column(name="LAST_ACCESS_DATE")
	private Date lastAccessDate;
	
	@Column(name="NUMBER_OF_ACCESS")
	private int numberOfAccess;

	public Long getShelfId() {
		return shelfId;
	}

	public void setShelfId(Long shelfId) {
		this.shelfId = shelfId;
	}

	public BookBean getBook() {
		return book;
	}

	public void setBook(BookBean book) {
		this.book = book;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}

	public Date getLastAccessDate() {
		return lastAccessDate;
	}

	public void setLastAccessDate(Date lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	public int getNumberOfAccess() {
		return numberOfAccess;
	}

	public void setNumberOfAccess(int numberOfAccess) {
		this.numberOfAccess = numberOfAccess;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((shelfId == null) ? 0 : shelfId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShelfBean other = (ShelfBean) obj;
		if (shelfId == null) {
			if (other.shelfId != null)
				return false;
		} else if (!shelfId.equals(other.shelfId))
			return false;
		return true;
	}
	
}
